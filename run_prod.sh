#!/bin/bash
export NODE_ENV=production
export NODE_PORT=8888
pm2 list
pm2 start app.js --name chatstik -e /root/logs/chatstik/error.log -o /root/logs/chatstik/output.log --log-date-format 'DD-MM HH:mm:ss.SSS'