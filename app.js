var cluster = require('cluster');
var path = require('path');
var winston = require(path.join(__dirname, "application", "utils", "winston"));

/*
if(cluster.isMaster) {
    var numWorkers = require('os').cpus().length;

    winston.info('Master cluster setting up ' + numWorkers + ' workers...');

    for(var i = 0; i < numWorkers; i++) {
        cluster.fork();
    }

    cluster.on('online', function(worker) {
        winston.info('Worker ' + worker.process.pid + ' is online');
    });

    cluster.on('exit', function(worker, code, signal) {
        winston.info('Worker ' + worker.process.pid + ' died with code: ' + code + ', and signal: ' + signal);
        winston.info('Starting a new worker');
        cluster.fork();
    });
} else {

}*/
/*START: module includes*/
var express = require('express');
var cors = require('cors');
var morgan = require('morgan');
var bodyParser = require('body-parser');
var compression = require('compression');
var ejs = require('ejs');
var morgan = require('morgan');
var mongoose = require('mongoose');
var methodOverride = require('method-override');
var winston = require(path.join(__dirname, "application", "utils", "winston"));
var config = require(path.join(__dirname, "application", "config", "config"));
var appRouter = require(path.join(__dirname, "application", "routes", "appRouter"));
var apiRouter = require(path.join(__dirname, "application", "routes", "apiRouter"));
var apiResponseHelper = require(path.join(__dirname, 'application', 'helpers', 'apiResponseHelper'));
var utilsHelper = require(path.join(__dirname, 'application', 'helpers', 'utilsHelper'));
var strangerChatController = require(path.join(__dirname, 'application', 'controllers', 'strangerChatController'));
var chatRoomController = require(path.join(__dirname, 'application', 'controllers', 'chatRoomController'));
/*END: module includes*/

var app = express();
var http = require('http').Server(app);
io = require('socket.io')(http);

io.on('connection', function(socket){
    if(socket.handshake.query.type === 'stranger_chat') {
        strangerChatController.initStrangerChat(socket);
    }
    else if(socket.handshake.query.type === 'chat_room') {
        chatRoomController.joinChatRoom(socket);
    }
});

/*START: middleware*/
app.use(cors());
app.use(compression());
app.use(morgan("dev", { "stream": winston.stream }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(methodOverride('X-HTTP-Method-Override'));
app.enable('trust proxy');
/*END: middleware*/

/*START: mongoose connection*/
mongoose.Promise = global.Promise;
mongoose.connect(config.database.url, config.database.options);
mongoose.connection.on('connected', function () {
    winston.info('Mongoose default connection open to ' + config.database.url);
});
mongoose.connection.on('error', function (err) {
    winston.info('Mongoose default connection error: ' + err);
    mongoose.disconnect();
});
mongoose.connection.on('disconnected', function () {
    winston.info('Mongoose default connection disconnected');
    process.exit(0);
});
process.on('SIGINT', function () {
    mongoose.connection.close(function ()
    {
        winston.info('Mongoose default connection disconnected through app termination');
        process.exit(0);
    });
});
/*END: mongoose connection*/

/*START: template engine*/
app.set('views', path.join(__dirname, 'application', 'templates'));
app.engine('html', ejs.renderFile);
app.set('view engine', 'html');
/*END: template engine*/

/*START: static routes served by nginx*/
app.use('/resources', express.static(path.join(__dirname, 'application', 'static', 'public')));
app.use('/manifest.json', express.static(path.join(__dirname, 'application', 'static', 'public', 'chatstik', 'scripts', 'lib', 'onesignal', 'manifest.json')));
app.use('/OneSignalSDKUpdaterWorker.js', express.static(path.join(__dirname, 'application', 'static', 'public', 'chatstik', 'scripts', 'lib', 'onesignal', 'OneSignalSDKUpdaterWorker.js')));
app.use('/OneSignalSDKWorker.js', express.static(path.join(__dirname, 'application', 'static', 'public', 'chatstik', 'scripts', 'lib', 'onesignal', 'OneSignalSDKWorker.js')));
/*END: static routes served by nginx*/

/*START: routes*/
app.use('/', appRouter);
app.use('/api/v1', apiRouter);
/*END: routes*/

/*START: handle 404 not found*/
app.use(function (req, res, next)
{
    apiResponseHelper.sendResponse(res, 404, false, "404 not found.");
});
/*END: handle 404 not found*/

/*START: handle exceptions*/
app.use(function (err, req, res, next)
{
    apiResponseHelper.sendResponse(res, err.status | 500, false, err.message, null, err.stack);
});
/*END: handle exceptions*/

/*START: listen to port*/
http.listen(config.server.port, function ()
{
    winston.info("server is up and running on port " + config.server.port + " in " + config.server.env + " mode");
});
/*END: listen to port*/
