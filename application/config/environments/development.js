var config = {};

/*START: database configuration*/
config.database = {};
config.database.type = 'mongodb';
config.database.dbname = 'chatstik';
config.database.username = 'admin';
config.database.password = 'admin';
config.database.host = "localhost";
config.database.port = "27017";
config.database.options =
{
    server: {
        socketOptions: {
            keepAlive: 1
        }
    }
};
config.database.url
    = config.database.type + "://" + config.database.username + ":" + config.database.password + "@" + config.database.host + ":" + config.database.port + "/" + config.database.dbname;
/*END: database configuration*/

/*START: json web token configuration*/
config.jwt = {};
config.jwt.secret = "7854jgkfdg854jgkfdjg895483jgkfdjgwqaseaads";
config.jwt.options = {};
config.jwt.options.expiresIn = 60 * 60; //in seconds
/*END: json web token configuration*/

module.exports = config;