/*START: module includes*/
var path = require('path');
/*END: module includes*/

var config = {};

//load environment specific configuration
config = require(path.join(__dirname, "environments", process.env.NODE_ENV || process.env.ENV || 'development'));

/*START: set server configuration based on environment variables*/
config.server = {};
config.server.env = process.env.NODE_ENV || process.env.ENV || 'development';
config.server.port = process.env.NODE_PORT || process.env.PORT || 8888;
/*END: set server configuration based on environment variables*/

/*START: app settings*/
config.app = {};
config.app.name = 'chatstik';
/*END: app settings*/

module.exports = config;
