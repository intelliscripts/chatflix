/*START: module includes*/
var appRouter = require('express').Router({mergeParams: true});
var path = require('path');
var config = require(path.join(__dirname, '..', 'config', 'config'));
var apiResponseHelper = require(path.join(__dirname, '..', 'helpers', 'apiResponseHelper'));
var utilsHelper = require(path.join(__dirname, '..', 'helpers', 'utilsHelper'));
var appController = require(path.join(__dirname, '..', 'controllers', 'appController'));
/*END: module includes*/

/*START: routes*/
appRouter.get('/', appController.home);
appRouter.get('/admin', appController.admin);
/*END: routes*/

module.exports = appRouter;