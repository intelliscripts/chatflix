/*START: module includes*/
var apiRouter = require('express').Router({mergeParams: true});
var path = require('path');
var config = require(path.join(__dirname, '..', 'config', 'config'));
var apiResponseHelper = require(path.join(__dirname, '..', 'helpers', 'apiResponseHelper'));
var utilsHelper = require(path.join(__dirname, '..', 'helpers', 'utilsHelper'));
var chatRoomController = require(path.join(__dirname, '..', 'controllers', 'chatRoomController'));
var adminController = require(path.join(__dirname, '..', 'controllers', 'adminController'));
var authController = require(path.join(__dirname, '..', 'controllers', 'authController'));
/*END: module includes*/

/*START: routes*/
apiRouter.post('/chatrooms', chatRoomController.create);
apiRouter.post('/admin/login', adminController.login);
apiRouter.post('/chatrooms', chatRoomController.create);
apiRouter.get('/chatrooms', chatRoomController.getAll);
apiRouter.get('/chatrooms/:id', chatRoomController.get);
apiRouter.delete('/admin/chatrooms/:id',authController.authenticateToken, adminController.deleteChatroom);
apiRouter.post('/admin/chatrooms', authController.authenticateToken, adminController.createChatroom);
/*END: routes*/

module.exports = apiRouter;