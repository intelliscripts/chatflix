/*START: module includes*/
var utilsHelper = {};
var bcrypt = require('bcrypt-nodejs');
var nodeUuid = require('node-uuid');
var mongoose = require('mongoose');
/*END: module includes*/

/*START: generateHash */
utilsHelper.generateHash = function (text) {
	return bcrypt.hashSync(text, bcrypt.genSaltSync(8), null);
};
/*END: generateHash */

/*START: generateRandomKey */
utilsHelper.generateRandomKey = function (isTimeBased) {
	if (isTimeBased) {
		return nodeUuid.v1();
	}

	return nodeUuid.v4();
};
/*END: generateRandomKey */

/*START: containsSpecialChars */
utilsHelper.containsSpecialChars = function(str) {

	if (!/[^a-zA-Z0-9]/.test(str)) {
		return false;
	}
	return true;
};
/*END: containsSpecialChars */

/*START: getMongooseError */
utilsHelper.getMongooseError = function (err) {
	var error = {};
	error.message = '';
	error.errorPayload = {};

	if (!err) {
		return error;
	}

	error.errorPayload.code = err.code;
	if (err.code == '11000') {
		error.message = 'record already exists';
	}
	else if (err.errors) {
		for (key in err.errors) {
			error.message = err.errors[key].name + "-" + err.errors[key].message;
			break;
		}
	}
	else {
		error.message = err.message ? err.name + " - " + err.message : err.name;
	}

	return error;
};
/*END: getMongooseError */

/*START: isValidObjectId */
utilsHelper.isValidObjectId = function (str)
{
	return mongoose.Types.ObjectId.isValid(str);
};
/*END: isValidObjectId */

module.exports = utilsHelper;