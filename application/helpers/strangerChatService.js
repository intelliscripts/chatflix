var path = require('path');
var utilsHelper = require(path.join(__dirname, 'utilsHelper'));
var strangerChatService = {};
var strangers = {};
var count = 0;

/*START: connectToStranger*/
strangerChatService.connectToStranger = function (socket)
{
	if (!strangers[socket.id]) {
		strangers[socket.id] = socket;
	}
	var connectionInfo = {
		length: Object.keys(strangers).length,
		id: socket.id
	};

	for (var key in strangers) {
		if (Object.keys(strangers).length > 1) {
			if (key) {
				var waitingSocket = strangers[key];
				count++;
				waitingSocket.join('room_' + count);
				socket.join('room_' + count);
				waitingSocket.room = 'room_' + count;
				socket.room = 'room_' + count;
				delete strangers[key];
				delete strangers[socket.id];
				socket.emit('join_room');
				waitingSocket.emit('join_room');
			}
		}
	}

	socket.on("disconnect",function(data){
		socket.broadcast.to(socket.room).emit('disconnect_from_room');
		delete strangers[socket.id];
	});

	socket.on("send_message",function(data){
		socket.broadcast.to(socket.room).emit('receive_message', data);
	});

	socket.on("update_location",function(data){
		socket.broadcast.to(socket.room).emit('update_location', data);
	});

	return connectionInfo;
};
/*END: connectToStranger*/


module.exports = strangerChatService;