var path = require('path');
var utilsHelper = require(path.join(__dirname, 'utilsHelper'));
var Chatroom = require(path.join(__dirname, '..', 'models', 'chatroom'));
var chatRoomService = {};
var chatRooms = {};
var count = 0;

/*START: create*/
chatRoomService.create = function (chatroom)
{
	var roomId = chatroom._id;
	chatRooms[roomId] = {};
	chatRooms[roomId].room = 'chat_room_' + count;
	chatRooms[roomId].name = chatroom.name;
	chatRooms[roomId].type = chatroom.type;
	chatRooms[roomId].password = chatroom.password;
	chatRooms[roomId].users = [];
	count++;
	return roomId;
};
/*END: create*/

/*START: get*/
chatRoomService.get = function (id)
{
	return chatRooms[id];
};
/*END: get*/

/*START: join*/
chatRoomService.join = function (socket)
{
	var roomId = socket.handshake.query.room_id;
	var chatRoom = chatRooms[roomId];
	function join(chatRoom) {
		socket.join(chatRoom.room);
		socket.room = chatRoom.room;
		var user = {};
		user.name = socket.handshake.query.user_name;
		user.socketId = socket.id;
		chatRoom.users.push(user);
		socket.emit('join_room', {room: chatRoom});
		socket.broadcast.to(socket.room).emit('new_joinee', {room: chatRoom});
		socket.on("send_message",function(data){
			socket.broadcast.to(socket.room).emit('receive_message', data);
		});
		socket.on("disconnect",function(data){
			for (var i = 0;i < chatRoom.users.length; i++) {
				if (socket.id === chatRoom.users[i].socketId) {
					chatRoom.users.splice(i, 1);
					break;
				}
			}
			socket.broadcast.to(socket.room).emit('disconnect_from_room', {room: chatRoom});
		});
	}
	if (chatRoom) {
		join(chatRoom);
	}
	else {
		Chatroom.findOne({_id: roomId}, function(err, chatroom) {
			if (err) {
				socket.emit('invalid_room');
			}
			else {
				if (chatroom) {
					chatRoomService.create(chatroom);
					join(chatRoomService.get(chatroom._id));
				}
				else {
					socket.emit('invalid_room');
				}
			}
		});
	}
};
/*END: join*/

chatRoomService.forceDisconnect = function(room) {
	console.log(room);
	io.sockets.in(room.room).emit('force_disconnect');
};

chatRoomService.delete = function(room) {
	chatRoomService.forceDisconnect(chatRoomService.get(room._id));
	delete chatRooms[room._id];
};

module.exports = chatRoomService;