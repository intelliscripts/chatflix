var winston = require('winston');
var path = require('path');
var config = require(path.join(__dirname, '..', 'config', 'config'));
winston.emitErrs = true;

var configuration = {};
configuration.transports = [];
configuration.exitOnError = false;

if(config.server.env == 'development') {
    var consoleTranspost = new winston.transports.Console({
        level: 'debug',
        handleExceptions: true,
        json: false,
        colorize: true
    });
    configuration.transports.push(consoleTranspost);
}
else if (config.server.env == 'production') {
    var fileTransport = new winston.transports.File({
        level: 'error',
        filename: 'logs/chatstik.log',
        json: false,
        maxsize: 5242880,
        colorize: false
    });
    winston.handleExceptions(new winston.transports.File({ filename: 'logs/exceptions.log' }));
    configuration.transports.push(fileTransport);
}

var logger = new winston.Logger(configuration);

module.exports = logger;
module.exports.stream = {
    write: function(message, encoding){
        logger.info(message);
    }
};
