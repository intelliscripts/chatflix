/*START: module includes*/
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
/*END: module includes*/

/*START: schema definition*/
var chatroomSchema = new Schema({
    name: {
        type: String,
        required: ['chatroom name is required.']
    },
    type: {
        type: String,
        required: ['chatroom type is required.'],
        default: 'public'
    },
    password: {
        type: String
    },
    created_by: {
        type: String
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});
/*END: schema definition*/

/*START:instantiate the model*/
var Chatroom = mongoose.model('chatroom', chatroomSchema);
/*END:instantiate the model*/

/*START: schema validations*/
Chatroom.schema.path('name').validate(function(name) {
    if (name.length < 6) {
        return false;
    }
    return true;
},'chatroom name should be more than 6 characters.');

Chatroom.schema.path('name').validate(function(name) {
    if (name.length > 100) {
        return false;
    }
    return true;
},'chatroom name should be less than 100 characters.');
/*END: schema validations*/

/*START: export schema*/
module.exports = Chatroom;
/*END: export schema*/