(function($) {
    'use strict';

    angular.module('chatstik_admin').controller('consoleController', ['$scope', 'authService', '$rootScope', '$state','notifyService','$http', function($scope, authService, $rootScope, $state, notifyService, $http) {
        function init() {
            $scope.newChatRoom = {};
            getAllChatrooms();
        }
        function getAllChatrooms() {
            $http({
                method: 'GET',
                url: '/api/v1/chatrooms'
            })
            .success(function(data) {
                $scope.chatrooms = data.payload;
            });
        }
        $scope.logout = function() {
            authService.logout();
            $state.go('login');
        };
        $scope.createChatroom = function() {
            if (!$scope.newChatRoom.name) {
                notifyService.show('danger', 'invalid room name.');
                return;
            }
            $http({
                method: 'POST',
                url: '/api/v1/admin/chatrooms',
                data: {
                    name: $scope.newChatRoom.name
                }
            })
            .success(function(data) {
                    if (data.result.success) {
                        $scope.newChatRoom = {};
                        $scope.chatrooms.push(data.payload);
                    }
                    else {
                        notifyService.show('danger', data.result.message);
                    }
            })
            .error(function(data) {
                    notifyService.show('danger', data.result.message);
            });
        };
        $scope.deleteChatroom = function(chatroom, index) {
            $http({
                method: 'DELETE',
                url: '/api/v1/admin/chatrooms/' + chatroom._id
            })
                .success(function(data) {
                    if (data.result.success) {
                        $scope.chatrooms.splice(index, 1);
                    }
                    else {
                        notifyService.show('danger', data.result.message);
                    }
                })
                .error(function(data) {
                    notifyService.show('danger', data.result.message);
                });
        };
        init();
    }]);

}(jQuery));