(function($) {
    'use strict';

    angular.module('chatstik_admin').controller('loginController', ['$scope', 'authService', '$rootScope', '$state','notifyService', function($scope, authService, $rootScope, $state, notifyService) {
        $scope.login = function(username, password) {
            if (!username) {
                notifyService.show('danger', 'invalid username.');
                return;
            }
            if (!password) {
                notifyService.show('danger', 'invalid password.');
                return;
            }

            authService.login(username, password).then(function() {
                $state.go('console');
            }, function(data) {
                notifyService.show('danger', data.result.message);
            });
        };
    }]);

}(jQuery));