(function($) {
    'use strict';

    var chatstik_admin = angular.module('chatstik_admin', ['ui.router', 'ui.bootstrap']);
    chatstik_admin.factory('authTokenInterceptor', ['$injector','$rootScope', function($injector, $rootScope) {
        var api = {};
        api.request = function (config)
        {
            $rootScope.loading ++;
            var authToken = $injector.get('authService').getAuthToken();
            if (authToken) {
                config.headers['Authorization'] = authToken;
            }
            return config;
        };
        api.requestError = function (config)
        {
            $rootScope.loading --;
            return config;
        };
        api.response = function(response) {
            $rootScope.loading --;
            return response;
        };
        api.responseError = function(response) {
            $rootScope.loading --;
            if (response.data.result && response.data.result.error && response.data.result.error.code === 'logout') {
                $injector.get('authService').logout();
                $injector.get('$state').go('login');
                response.data.result.message = 'session timed out.';
            }
            return response;
        };
        return api;
    }]);
    chatstik_admin.config(['$stateProvider', '$urlRouterProvider', '$httpProvider','$locationProvider', function ($stateProvider, $urlRouterProvider, $httpProvider, $locationProvider) {
        $httpProvider.interceptors.push('authTokenInterceptor');
        $stateProvider
            .state('login',
            {
                url: '/login',
                templateUrl: '/resources/chatstik/scripts/admin/templates/login.html',
                controller: 'loginController'
            })
            .state('console',
            {
                url: '/console',
                templateUrl: '/resources/chatstik/scripts/admin/templates/console.html',
                controller: 'consoleController',
                requireAuthentication: true
            });
        $urlRouterProvider.otherwise('/login');
    }]);
    chatstik_admin.run(['$rootScope', '$state','authService', function($rootScope, $state, authService) {
        $rootScope.loading = 0;
        $rootScope.server = server;
        authService.tryLoginFromSession();
        /*$stateChangeStart*/
        $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
            if (toState.requireAuthentication) {
                if (!authService.isLogged()) {
                    event.preventDefault();
                    $state.go('login');
                }
            }
            if (toState.redirectTo) {
                event.preventDefault();
                $state.go(toState.redirectTo, toParams, {location: 'replace'})
            }
        });
        /*$stateChangeStart*/
    }]);

}(jQuery));
