(function($) {
    'use strict';

    angular.module('chatstik_admin').factory('notifyService', [function() {
        var api = {};
        var config = {};
        config.delay = 3000;

        api.show = function(type, message, delay) {
            $.notify({
                message: message
            },{
                type: type,
                delay: delay || config.delay,
                placement: {
                    from: "top",
                    align: "center"
                },
                z_index: 9999
            });
        };

        return api;
    }]);

}(jQuery));