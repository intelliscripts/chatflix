(function($) {
    'use strict';

    angular.module('chatstik_admin').factory('authService', ['$http','$q','$rootScope', function($http, $q, $rootScope) {
        var api = {};
        var logged = false;
        var authToken = null;

        api.login = function(username, password) {
            var payload = {};
            payload.username = username;
            payload.password = password;

            var deferred = $q.defer();
            $http(
                {
                    method: 'POST',
                    data: payload,
                    url: '/api/v1/admin/login'
                }).success(function (data, status, headers, config)
                {
                    if (data.result.success) {
                        logged = true;
                        authToken = data.payload;
                        localStorage.setItem('authToken', authToken);
                        deferred.resolve();
                    }
                    else {
                        deferred.reject(data);
                    }
                }).error(function (data, status, headers, config)
                {
                    deferred.reject(data);
                });
            return deferred.promise;
        };

        api.isLogged = function() {
            return logged;
        };

        api.getAuthToken = function() {
            if (logged) {
                return authToken;
            }
            return null;
        };

        api.logout = function() {
            logged = false;
            authToken = null;
            localStorage.removeItem('authToken');
        };

        api.tryLoginFromSession = function() {
            if (localStorage.getItem('authToken')) {
                logged = true;
                authToken = localStorage.getItem('authToken')
            }
        };

        return api;
    }]);

}(jQuery));