(function($) {
    'use strict';

    angular.module('chatstik').controller('rootController', ['$scope','userService', function($scope, userService) {
        $scope.userService = userService;
        $scope.$watch('userService.user', function(newVal, oldVal) {
            $scope.user = angular.copy(newVal);
        }, true);
    }]);

}(jQuery));