(function($) {
    'use strict';

    angular.module('chatstik').controller('chatController', ['$scope','$timeout','userService','audioPlayer','utilHelpers','$window', function($scope, $timeout, userService, audioPlayer, utilHelpers, $window) {
        var socket;
        $scope.connection = {};
        $scope.messages = [];
        $scope.message = {};
        $scope.stranger = {};
        $scope.connect = function() {
            $scope.messages = [];
            $scope.connection.status = 'waiting';
            socket = io({query: {
                type: 'stranger_chat'
            }});
            socket.on('join_room', function(){
                $scope.$apply(function() {
                    userService.getLocation().then(function(data) {
                        socket.emit('update_location', data);
                    });
                    $scope.connection.status = 'connected';
                });
            });
            socket.on('update_location', function(data){
                $scope.$apply(function() {
                    $scope.stranger.location = data;
                });
            });
            socket.on('disconnect_from_room', function(data){
                socket.disconnect();
                $scope.$apply(function() {
                    $scope.connection.status = 'disconnected';
                    $scope.stranger = {};
                    $scope.message = {};
                    autoScroll();
                });
            });
            socket.on('receive_message', function(message){
                $scope.$apply(function() {
                    audioPlayer.playAudio(audioPlayer.types.NEW_MESSAGE);
                    message.from = 'stranger';
                    $scope.messages.push(message);
                    autoScroll();
                });
            });
        };
        $scope.disconnect = function() {
            socket.disconnect();
            $scope.connection.status = 'disconnected';
            $scope.message = {};
            $scope.stranger = {};
            autoScroll();
        };
        $scope.sendMessage = function() {
            if (!$scope.message.text) {
                return;
            }
            $scope.message.type = 'text';
            if (utilHelpers.isURL($scope.message.text)) {
                $scope.message.type = 'link';
            }
            send();
        };

        $scope.sendImage = function(image) {
            if (!image) {
                return;
            }
            $scope.message.type = 'image';
            $scope.message.image = image;
            send();
        };
        $scope.openImage = function(url) {
            $window.open(url, '_blank');
        };
        function send() {
            $scope.message.date = new Date().toString();
            $scope.message.from = 'me';
            $scope.messages.push($scope.message);
            socket.emit('send_message', $scope.message);
            $scope.message = {};
            autoScroll();
        }
        function autoScroll() {
            $timeout(function() {
                angular.element('.chat_box_body').scrollTop(angular.element('.chat_box_body')[0].scrollHeight + 400);
            });
        }
        $scope.$on('$destroy', function () {
            if (socket) {
                socket.disconnect();
            }
        });
        function init() {
            $scope.connect();
        }
        init();
    }]);

}(jQuery));