(function($) {
    'use strict';

    angular.module('chatstik').controller('chatRoomController', ['$scope','$timeout','$stateParams','userService','chatRoomService','$uibModal','audioPlayer','$location','utilHelpers','$window',function($scope, $timeout, $stateParams, userService, chatRoomService, $uibModal, audioPlayer, $location, utilHelpers, $window) {
        var socket;
        $scope.connection = {};
        $scope.messages = [];
        $scope.message = {};
        $scope.chatRoom = {};
        $scope.chatRoom.url = $location.absUrl();
        $scope.chatRoom.valid = true;
        $scope.chatRoom.id = $stateParams.id;
        $scope.chatRoom.users = [];
        $scope.$watch('userService.user', function(newVal, oldVal) {
            $scope.user = angular.copy(newVal);
        }, true);
        $scope.connect = function() {
            connect();
        };
        function connect() {
            $scope.messages = [];
            $scope.connection.status = 'waiting';
            socket = io({query: {
                type: 'chat_room',
                room_id: $scope.chatRoom.id,
                user_name: $scope.user.name
            }});
            socket.on('join_room', function(data){
                $scope.$apply(function() {
                    var room = data.room;
                    $scope.connection.status = 'connected';
                    $scope.chatRoom.users = room.users;
                });
            });
            socket.on('new_joinee', function(data){
                $scope.$apply(function() {
                    var room = data.room;
                    $scope.chatRoom.users = room.users;
                });
            });
            socket.on('disconnect_from_room', function(data){
                $scope.$apply(function() {
                    var room = data.room;
                    $scope.chatRoom.users = room.users;
                });
            });
            socket.on('invalid_room', function(){
                $scope.$apply(function() {
                    $scope.chatRoom.valid = false;
                    $scope.connection.status = 'disconnected';
                });
            });
            socket.on('force_disconnect', function(){
                $scope.disconnect();
            });
            socket.on('receive_message', function(message){
                $scope.$apply(function() {
                    if (!message.from) {
                        message.from = 'stranger';
                    }
                    $scope.messages.push(message);
                    audioPlayer.playAudio(audioPlayer.types.NEW_MESSAGE);
                    autoScroll();
                });
            });
        }
        $scope.disconnect = function() {
            $scope.chatRoom.users = [];
            socket.disconnect();
            $scope.connection.status = 'disconnected';
            $scope.message = {};
            autoScroll();
        };
        $scope.sendMessage = function() {
            if (!$scope.message.text) {
                return;
            }
            $scope.message.type = 'text';
            if (utilHelpers.isURL($scope.message.text)) {
                $scope.message.type = 'link';
            }
            send();
        };

        $scope.sendImage = function(image) {
            if (!image) {
                return;
            }
            $scope.message.type = 'image';
            $scope.message.image = image;
            send();
        };
        $scope.openImage = function(url) {
            $window.open(url, '_blank');
        };
        function send() {
            $scope.message.date = new Date().toString();
            $scope.message.from = $scope.user.name;
            $scope.messages.push($scope.message);
            socket.emit('send_message', $scope.message);
            $scope.message = {};
            autoScroll();
        }

        function autoScroll() {
            $timeout(function() {
                angular.element('.chat_box_body').scrollTop(angular.element('.chat_box_body')[0].scrollHeight + 400);
            });
        }
        $scope.$on('$destroy', function () {
            if (socket) {
                socket.disconnect();
            }
        });
        function init() {
            chatRoomService.get($scope.chatRoom.id).then(function(response) {
                var data = response.data;
                if (data.result.success) {
                    var room = data.payload;
                    $scope.chatRoom.valid = true;
                    $scope.chatRoom.name = room.name;
                    $scope.chatRoom.type = room.type;
                    if ($scope.chatRoom.type === 'private') {
                        var modal = $uibModal.open({
                            templateUrl: 'verifyChatRoomPassword.html',
                            controller: function($scope, chatRoom, userService) {
                                $scope.close = function() {
                                    modal.close();
                                };
                                $scope.continue = function() {
                                    if ($scope.password && $scope.password === chatRoom.password) {
                                        $scope.incorrectPassword = false;
                                        $scope.close();
                                        userService.askUserName().then(function() {
                                            $timeout(function() {
                                                connect();
                                            });
                                        });
                                    }
                                    else {
                                        $scope.incorrectPassword = true;
                                    }
                                };
                            },
                            size: 'md',
                            resolve: {
                                chatRoom: function() {
                                    return room;
                                }
                            }
                        });
                    }
                    else {
                        userService.askUserName().then(function() {
                            $timeout(function() {
                                connect();
                            });
                        });
                    }
                }
                else {
                    $scope.chatRoom.valid = false;
                    $scope.connection.status = 'disconnected';
                }
            });
        }
        init();
    }]);

}(jQuery));