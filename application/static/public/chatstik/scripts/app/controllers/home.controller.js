(function($) {
    'use strict';

    angular.module('chatstik').controller('homeController', ['$scope','$uibModal', function($scope, $uibModal) {
        $scope.agreed = true;
        var options = {
            strings: ["Great way to meet new friends...", "Chat with strangers in private chat rooms...", "The chat service is completely anonymous...", "Public chat rooms...", "No registration needed..."],
            typeSpeed: 40
        };

        var typed = new Typed(".type_introduction", options);
        $scope.openChatRoomModal = function() {
            var modal = $uibModal.open({
                templateUrl: 'createChatRoom.html',
                controller: function($scope, $http, $state, utilHelpers) {
                    $scope.chatRoom = {};
                    $scope.chatRoom.type = 'public';
                    $scope.close = function() {
                      modal.close();
                    };
                    $scope.createChatRoom = function() {
                        if (!$scope.chatRoom.name) {
                            return;
                        }
                        if ($scope.chatRoom.type === 'private') {
                            if (!$scope.chatRoom.password) {
                                return;
                            }
                        }
                        $http({
                            method: 'POST',
                            url: '/api/v1/chatrooms',
                            data: {
                                name: $scope.chatRoom.name,
                                type: $scope.chatRoom.type,
                                password: $scope.chatRoom.password
                            }
                        })
                        .success(function(data) {
                            $scope.chatRoom.id = data.payload;
                            $scope.chatRoom.created = true;
                            $scope.chatRoom.url = utilHelpers.getHost() + '/#/chatroom/' + $scope.chatRoom.id;
                        });
                    };
                    $scope.joinChatRoom = function() {
                        $scope.close();
                        $state.go('chatroom', {id: $scope.chatRoom.id});
                    };
                    $scope.copyURL = function() {
                        window.prompt("Copy to clipboard: Ctrl+C, Enter", $scope.chatRoom.url);
                    };
                },
                size: 'md',
                resolve: {
                }
            });
        };
        $scope.joinChatRoomModal = function() {
            var modal = $uibModal.open({
                templateUrl: 'joinChatRoom.html',
                controller: function($scope, $state, $http) {
                    function getAllChatrooms() {
                        $http({
                            method: 'GET',
                            url: 'api/v1/chatrooms'
                        })
                        .success(function(data) {
                            $scope.chatrooms = data.payload;
                        });
                    }
                    $scope.close = function() {
                        modal.close();
                    };
                    $scope.join = function(chatRoom) {
                        $scope.close();
                        $state.go('chatroom', {id: chatRoom._id});
                    };
                    getAllChatrooms();
                },
                size: 'md',
                resolve: {
                }
            });
        };
    }]);

}(jQuery));