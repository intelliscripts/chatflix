(function($) {
    'use strict';

    angular.module('chatstik').directive('autoSizeChatBox', ['$timeout','$window', function($timeout, $window) {
        return {
            restrict: 'EAC',
            scope: {
            },
            link: function($scope, $element) {
                $timeout(function() {
                    var headerHeight = angular.element('.chatstik header.app_header').outerHeight();
                    var windowHeight = angular.element($window).height();
                    var chatBoxHeaderHeight = angular.element('.chat_box_header').outerHeight();
                    var chatBoxFooterHeight = angular.element('.chat_box_footer').outerHeight();
                    var availableHeight = windowHeight - headerHeight - chatBoxHeaderHeight - chatBoxFooterHeight - 30;
                    angular.element($element).find('.chat_box_body').css('height', availableHeight + 'px');
                    angular.element($element).find('.chatroom_users_body').css('height', windowHeight - headerHeight - chatBoxHeaderHeight - 30 + 'px');
                });
            }
        };
    }]);

}(jQuery));