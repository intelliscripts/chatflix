(function($) {
    'use strict';

    angular.module('chatstik').directive('sendImage', ['$uibModal',function($uibModal) {
        return {
            restrict: 'EAC',
            templateUrl: '/resources/chatstik/scripts/app/directives/templates/sendImage.html',
            scope: {
                callback: '&'
            },
            link: function($scope, $element) {
            },
            controller: function($scope) {
                function callback(image) {
                    $scope.callback(image);
                }
                $scope.openModal = function(type) {
                    if (type === 'url') {
                        fromUrl(type);
                    }
                    if (type === 'cam') {
                        fromCam(type);
                    }
                };
                function fromCam(type) {
                    var modal = $uibModal.open({
                        templateUrl: 'imageFromCam.html',
                        controller: function($scope, $timeout) {
                            navigator.getUserMedia = ( navigator.getUserMedia ||
                            navigator.webkitGetUserMedia ||
                            navigator.mozGetUserMedia ||
                            navigator.msGetUserMedia);
                            var video;
                            var cameraStream;
                            $scope.image = {};
                            $scope.image.type = type;
                            $scope.close = function() {
                                if (cameraStream) {
                                    cameraStream.getVideoTracks()[0].stop();
                                }
                                if (video) {
                                    video.src="";
                                }
                                if ($scope.ctx) {
                                    $scope.ctx.clearRect(0,0,320,240);
                                }
                                modal.close();
                            };
                            $scope.send = function() {
                                $scope.image.url = $scope.canvas.toDataURL();
                                callback({
                                    image: $scope.image
                                });
                                $scope.close();
                            };
                            $scope.startWebcam = function() {
                                if (navigator.getUserMedia) {
                                    $scope.supported = true;
                                    navigator.getUserMedia ({video: true},
                                        function(localMediaStream) {
                                            $scope.$apply(function() {
                                                $scope.permission = true;
                                            });
                                            video = document.querySelector('#webcam');
                                            video.src = window.URL.createObjectURL(localMediaStream);
                                            cameraStream = localMediaStream;
                                            video.play();
                                        },
                                        function(err) {
                                            $scope.permission = false;
                                            console.log("The following error occured: " + err);
                                        }
                                    );
                                } else {
                                    $scope.supported = false;
                                    console.log("getUserMedia not supported");
                                }
                            };
                            $scope.snapshot = function() {
                                $scope.snapshotTaken = true;
                                $scope.canvas = document.getElementById("snapshot");
                                $scope.ctx = $scope.canvas.getContext('2d');
                                $scope.canvas.style.width = '320px';
                                $scope.canvas.style.height = '240px';
                                $scope.ctx.drawImage(video, 0,0, 320, 240);
                            };
                            $scope.resetSnapshot = function() {
                                $scope.snapshotTaken = false;
                            };
                            $timeout(function() {
                                $scope.startWebcam();
                            });
                        },
                        windowClass: 'imageFromModal',
                        size: 'md'
                    });
                }
                function fromUrl(type) {
                    var modal = $uibModal.open({
                        templateUrl: 'imageFromURL.html',
                        controller: function($scope) {
                            $scope.image = {};
                            $scope.image.type = type;
                            $scope.close = function() {
                                modal.close();
                            };
                            $scope.send = function() {
                                callback({
                                    image: $scope.image
                                });
                                $scope.close();
                            };
                        },
                        windowClass: 'imageFromModal',
                        size: 'md'
                    });
                }
            }
        };
    }]);

}(jQuery));