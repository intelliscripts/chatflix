(function($) {
    'use strict';

    angular.module('chatstik').factory('utilHelpers', ['$location', function($location) {
        var api = {};

        api.getHost = function() {
            if ($location.port() == 80) {
                return $location.protocol() + '://'+ $location.host();
            }
            return $location.protocol() + '://'+ $location.host() +':'+  $location.port();
        };

        api.isURL = function(str) {
            return /^(https?):\/\/((?:[a-z0-9.-]|%[0-9A-F]{2}){3,})(?::(\d+))?((?:\/(?:[a-z0-9-._~!$&'()*+,;=:@]|%[0-9A-F]{2})*)*)(?:\?((?:[a-z0-9-._~!$&'()*+,;=:\/?@]|%[0-9A-F]{2})*))?(?:#((?:[a-z0-9-._~!$&'()*+,;=:\/?@]|%[0-9A-F]{2})*))?$/i.test(str);
        };

        return api;
    }]);

}(jQuery));