(function($) {
    'use strict';

    angular.module('chatstik').factory('audioPlayer', [function() {
        var api = {};
        api.types = {};
        api.types.NEW_MESSAGE = 'new_message';

       api.playAudio = function(type) {
           if (api.types.NEW_MESSAGE === type) {
               var audio = new Audio("/resources/chatstik/audio/message.mp3");
               audio.play();
           }
       };

        return api;
    }]);

}(jQuery));