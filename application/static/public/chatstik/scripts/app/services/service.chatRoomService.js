(function($) {
    'use strict';

    angular.module('chatstik').factory('chatRoomService', ['$q','$http', function($q, $http) {
        var api = {};

        api.get = function(id) {
            return $http({
                method: 'GET',
                url: '/api/v1/chatrooms/' + id
            });
        };

        return api;
    }]);

}(jQuery));