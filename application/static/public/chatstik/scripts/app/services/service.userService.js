(function($) {
    'use strict';

    angular.module('chatstik').factory('userService', ['$uibModal','$q', function($uibModal, $q) {
        var api = {};
        api.location = {};

        api.askUserName = function() {
            var deferred = $q.defer();
            var modal = $uibModal.open({
                templateUrl: 'askUserName.html',
                controller: function($scope) {
                    $scope.user = angular.copy(api.user);
                    $scope.close = function() {
                        modal.close();
                    };
                    $scope.save = function() {
                        api.user = $scope.user;
                        $scope.close();
                        deferred.resolve();
                    };
                },
                size: 'md',
                resolve: {
                }
            });
            return deferred.promise;
        };

        api.getLocation = function() {
            var deferred = $q.defer();
            if (api.location.fetched) {
                deferred.resolve(api.location);
            }
            else {
                jQuery.getJSON('//freegeoip.net/json/?callback=?', function(data) {
                    api.location = JSON.parse(JSON.stringify(data, null, 2));
                    api.location.fetched = true;
                    deferred.resolve(api.location);
                });
            }
            return deferred.promise;
        };

        function init() {
        }

        //init();
        api.getLocation();
        return api;
    }]);

}(jQuery));