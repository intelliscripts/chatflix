(function($) {
    'use strict';

    var chatstik = angular.module('chatstik', ['ui.router', 'ui.bootstrap']);
    chatstik.config(['$stateProvider', '$urlRouterProvider', '$httpProvider','$locationProvider', function ($stateProvider, $urlRouterProvider, $httpProvider, $locationProvider) {

        $stateProvider
            .state('home',
            {
                url: '/home',
                templateUrl: '/resources/chatstik/scripts/app/templates/home.html',
                controller: 'homeController'
            })
            .state('chat',
            {
                url: '/chat',
                templateUrl: '/resources/chatstik/scripts/app/templates/chat.html',
                controller: 'chatController'
            })
            .state('chatroom',
            {
                url: '/chatroom/:id',
                templateUrl: '/resources/chatstik/scripts/app/templates/chatroom.html',
                controller: 'chatRoomController'
            });
        $urlRouterProvider.otherwise('/home');
    }]);
    chatstik.run(['$rootScope', '$state', function($rootScope, $state) {
    }]);

}(jQuery));
