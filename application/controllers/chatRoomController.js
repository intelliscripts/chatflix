/*START: module includes*/
var chatRoomController = {};
var path = require('path');
var config = require(path.join(__dirname, '..', 'config', 'config'));
var chatRoomService = require(path.join(__dirname, '..', 'helpers', 'chatRoomService'));
var apiResponseHelper = require(path.join(__dirname, '..', 'helpers', 'apiResponseHelper'));
var Chatroom = require(path.join(__dirname, '..', 'models', 'chatroom'));
var utilsHelper = require(path.join(__dirname, '..', 'helpers', 'utilsHelper'));
/*END: module includes*/

/*START: create*/
chatRoomController.create = function(req, res, next) {
    if (!req.body.name) {
        apiResponseHelper.sendResponse(res, 400, false, "Chat room name is empty.");
        return;
    }
    if (req.body.type === 'private' && !req.body.password) {
        apiResponseHelper.sendResponse(res, 400, false, "Private room needs a password.");
        return;
    }
    req.body.created_by = 'user';
    var chatroom = new Chatroom(req.body);
    chatroom.save(function(err, chatroom) {
        if (err) {
            var mongooseError = utilsHelper.getMongooseError(err);
            apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
        }
        else {
            apiResponseHelper.sendResponse(res, 201, true, 'created successfully.', chatroom);
        }
    });
};
/*END: create*/

/*START: get*/
chatRoomController.get = function(req, res, next) {
    if (!req.params.id) {
        apiResponseHelper.sendResponse(res, 400, false, "invalid chatroom id.");
        return;
    }
    Chatroom.findOne({_id: req.params.id}, function(err, chatroom) {
        if (err) {
            var mongooseError = utilsHelper.getMongooseError(err);
            apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
        }
        else {
            if (chatroom) {
                if (chatRoomService.get(chatroom._id)) {
                    chatroom.set( 'users',chatRoomService.get(chatroom._id).users, { strict: false });
                }
                apiResponseHelper.sendResponse(res, 200, true, 'chatroom details', chatroom);
            }
            else {
                apiResponseHelper.sendResponse(res, 200, false, 'invalid chat room id.');
            }
        }
    });
};
/*END: get*/

/*START: joinChatRoom*/
chatRoomController.joinChatRoom = function(socket) {
    chatRoomService.join(socket);
};
/*END: joinChatRoom*/

chatRoomController.getAll = function(req, res, next) {
    Chatroom.find({}, function(err, chatrooms) {
        if (err) {
            var mongooseError = utilsHelper.getMongooseError(err);
            apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
        }
        else {
            for (var i = 0;i<chatrooms.length;i++) {
                var chatroom = chatrooms[i];
                if (chatRoomService.get(chatroom._id)) {
                    chatroom.set( 'users',chatRoomService.get(chatroom._id).users, { strict: false });
                }
                else {
                    chatroom.set( 'users',[], { strict: false });
                }
            }
            apiResponseHelper.sendResponse(res, 200, true, 'chatrooms', chatrooms);
        }
    });
};

module.exports = chatRoomController;