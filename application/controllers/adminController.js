/*START: module includes*/
var adminController = {};
var path = require('path');
var config = require(path.join(__dirname, '..', 'config', 'config'));
var apiResponseHelper = require(path.join(__dirname, '..', 'helpers', 'apiResponseHelper'));
var jwt = require('jsonwebtoken');
var Chatroom = require(path.join(__dirname, '..', 'models', 'chatroom'));
var utilsHelper = require(path.join(__dirname, '..', 'helpers', 'utilsHelper'));
var chatRoomService = require(path.join(__dirname, '..', 'helpers', 'chatRoomService'));
/*END: module includes*/

/*START: login*/
adminController.login = function(req, res, next) {
    if (!req.body.username) {
        apiResponseHelper.sendResponse(res, 400, false, 'username is required.');
        return;
    }
    if (!req.body.password) {
        apiResponseHelper.sendResponse(res, 400, false, 'password is required.');
        return;
    }
    if (req.body.username == 'admin' && req.body.password == 'admin') {
        //user payload in token
        var userPayload = {};
        userPayload.name = 'Admin';
        jwt.sign(userPayload, config.jwt.secret, config.jwt.options, function(err, authToken) {
            apiResponseHelper.sendResponse(res, 200, true, 'login successful', authToken);
        });
    }
    else {
        apiResponseHelper.sendResponse(res, 400, false, 'invalid credentials.');
    }
};
/*END: login*/
adminController.createChatroom = function(req, res, next) {
    if (!req.body.name) {
        apiResponseHelper.sendResponse(res, 400, false, 'chatroom name is required.');
        return;
    }
    req.body.created_by = 'admin';
    var chatroom = new Chatroom(req.body);
    chatroom.save(function(err, chatroom) {
        if (err) {
            var mongooseError = utilsHelper.getMongooseError(err);
            apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
        }
        else {
            apiResponseHelper.sendResponse(res, 201, true, 'created successfully.', chatroom);
        }
    });
};

adminController.deleteChatroom = function(req, res, next) {
    if (!req.params.id) {
        apiResponseHelper.sendResponse(res, 400, false, "invalid chatroom id.");
        return;
    }
    Chatroom.findOne({_id: req.params.id}, function(err, chatroom) {
        if (err) {
            var mongooseError = utilsHelper.getMongooseError(err);
            apiResponseHelper.sendResponse(res, 400, false, mongooseError.message, null, mongooseError.errorPayload);
        }
        else {
            if (chatroom) {
                chatroom.remove();
                chatRoomService.delete(chatroom);
                apiResponseHelper.sendResponse(res, 200, true, 'chatroom deleted.');
            }
            else {
                apiResponseHelper.sendResponse(res, 200, false, 'invalid chat room id.');
            }
        }
    });
};

module.exports = adminController;