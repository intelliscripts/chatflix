/*START: module includes*/
var appController = {};
var path = require('path');
var config = require(path.join(__dirname, '..', 'config', 'config'));
/*END: module includes*/

/*START: home*/
appController.home = function(req, res, next) {
    res.render('app/home');
};
/*END: home*/
/*START: admin*/
appController.admin = function(req, res, next) {
    var host = req.protocol + '://' + req.headers.host;
    var api = host + '/api/v1';
    res.render('app/admin', {host: host, api:api});
};
/*END: admin*/

module.exports = appController;