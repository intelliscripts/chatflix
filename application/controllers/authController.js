/*START: module includes*/
var authController = {};
var path = require('path');
var jwt = require('jsonwebtoken');
var config = require(path.join(__dirname, '..', 'config', 'config'));
var apiResponseHelper = require(path.join(__dirname, '..', 'helpers', 'apiResponseHelper'));
var utilsHelper = require(path.join(__dirname, '..', 'helpers', 'utilsHelper'));
/*END: module includes*/

/*START: authenticateToken*/
authController.authenticateToken = function(req, res, next) {
    var token = req.headers.authorization;
    if (!token) {
        var errorPayload = {};
        errorPayload.message = 'authorization header is required.';
        errorPayload.code = 'logout';
        apiResponseHelper.sendResponse(res, 401, false, 'unauthorized access.', null, errorPayload);
        return;
    }

    jwt.verify(token, config.jwt.secret, function (err, decoded)
    {
        if (err)
        {
            err.code = 'logout';
            apiResponseHelper.sendResponse(res, 401, false, 'unauthorized access.', null, err);

        }
        else
        {
            req.key = decoded;
            next();
        }
    });
};
/*END: authenticateToken*/

module.exports = authController;