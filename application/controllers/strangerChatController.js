/*START: module includes*/
var strangerChatController = {};
var path = require('path');
var config = require(path.join(__dirname, '..', 'config', 'config'));
var strangerChatService = require(path.join(__dirname, '..', 'helpers', 'strangerChatService'));
/*END: module includes*/

/*START: initStrangerChat*/
strangerChatController.initStrangerChat = function(socket) {
    strangerChatService.connectToStranger(socket);
};
/*END: initStrangerChat*/

module.exports = strangerChatController;